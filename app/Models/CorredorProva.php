<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CorredorProva extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'corredores_provas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'corredores_corridas_id',
        'provas_id',
    ];

    public function corredoresCorridas()
    {
        return $this->belongsTo('App\Models\CorredorProva');
    }

    public function provas()
    {
        return $this->belongsTo('App\Models\Prova');
    }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
