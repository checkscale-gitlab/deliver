<?php

namespace App\Http\Controllers;

use App\Models\Prova;
use Illuminate\Http\Request;

class ProvasController extends Controller
{
    public function index()
    {
        $provas = Prova::get();
        return response()->json($provas);
    }

    public function store(Request $request)
    {
        $prova = new Prova();
        $prova->fill($request->all());
        $prova->save();

        return response()->json($prova, 201);
    }

    public function show($id)
    {
        $prova = Prova::find($id);

        if(!$prova) {
            return response()->json([
                'message'   => 'Registro não encontrado',
            ], 404);
        }

        return response()->json($prova);
    }

    public function update(Request $request, $id)
    {
        $prova = Prova::find($id);

        if(!$prova) {
            return response()->json([
                'message'   => 'Registro não encontrado',
            ], 404);
        }

        $prova->fill($request->all());
        $prova->save();

        return response()->json($prova);
    }

    public function destroy($id)
    {
        $prova = Prova::find($id);

        if(!$prova) {
            return response()->json([
                'message'   => 'Registro não encontrado',
            ], 404);
        }

        $prova->delete();
    }
}
